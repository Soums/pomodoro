const displayWork = document.querySelector('.displayWork')
const displayBreak = document.querySelector('.displayBreak')
const btnLaunch = document.querySelector('.b1')
const btnBreak = document.querySelector('.b2')
const btnReset = document.querySelector('.b3')
const cycles = document.querySelector('h2')

let timeInit = 2700;
let timeBreak = 600;
let stop = false;
let nbrCycles = 0;
let checkInterval = false;

cycles.innerText = `Nombre de cycles : ${nbrCycles}`

displayWork.innerText = `${Math.trunc(timeInit/60)} : ${(timeInit % 60 < 10) ? `0${timeInit % 60}` : timeInit %  60}`;
displayBreak.innerText = `${Math.trunc(timeBreak/60)} : ${(timeBreak % 60 < 10) ? `0${timeBreak % 60}` : timeBreak %  60}`;

btnLaunch.addEventListener('click', () => {
    if(checkInterval === false) {
        checkInterval = true;

    timeInit--;
    displayWork.innerText = `${Math.trunc(timeInit/60)} : ${(timeInit % 60 < 10) ? `0${timeInit % 60}` : timeInit %  60}`;

    let timer = setInterval(() => {
        if(stop === false && timeInit > 0) {
            timeInit--;
            displayWork.innerText = `${Math.trunc(timeInit/60)} : ${(timeInit % 60 < 10) ? `0${timeInit % 60}` : timeInit %  60}`;
        }
        else if(stop === false && timeBreak === 0 && timeInit === 0) {
            timeInit = 2700;
            timeBreak = 600;
            nbrCycles++;
            cycles.innerText = `Nombre de cycles : ${nbrCycles}`
            displayWork.innerText = `${Math.trunc(timeInit/60)} : ${(timeInit % 60 < 10) ? `0${timeInit % 60}` : timeInit %  60}`;
            displayBreak.innerText = `${Math.trunc(timeBreak/60)} : ${(timeBreak % 60 < 10) ? `0${timeBreak % 60}` : timeBreak %  60}`;
        }
        else if(stop === false && timeInit === 0) {
            timeBreak--;
            displayBreak.innerText = `${Math.trunc(timeBreak/60)} : ${(timeBreak % 60 < 10) ? `0${timeBreak % 60}` : timeBreak %  60}`;
        }
    }, 1000)

    btnReset.addEventListener('click', () => {
        clearInterval(timer)
        checkInterval = false
        timeInit = 2700
        timeBreak = 600
        displayWork.innerText = `${Math.trunc(timeInit/60)} : ${(timeInit % 60 < 10) ? `0${timeInit % 60}` : timeInit %  60}`;
        displayBreak.innerText = `${Math.trunc(timeBreak/60)} : ${(timeBreak % 60 < 10) ? `0${timeBreak % 60}` : timeBreak %  60}`;
    })

    } else {
        return
    }
})

btnBreak.addEventListener('click', () => {
    if(stop === false){
        btnBreak.innerText = "Play"
    } else if( stop === true) {
       btnBreak.innerText = "Pause" 
    }
    stop = !stop;
})